package web.controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import web.models.Hit;
import web.models.HitList;

import java.io.IOException;
import java.util.Arrays;

@WebServlet(name = "AreaCheckServlet", value = "/AreaCheckServlet")
public class AreaCheckServlet extends HttpServlet {

    HitList hitList;

    public void init(){
        hitList = new HitList();
        getServletConfig().getServletContext().setAttribute("list", hitList);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    try {
        long time = System.nanoTime();
        String xNot = req.getParameter("x");
        String yNot = req.getParameter("y");
        String rNot = req.getParameter("r");

        validate(xNot, yNot, rNot);

        int x = Integer.parseInt(xNot);
        double y = Double.parseDouble(yNot);
        int r = Integer.parseInt(rNot);

        Hit hit =  new Hit(x,y,r,System.nanoTime() - time);
        int id = hitList.add(hit);
        req.setAttribute("id", id);
        req.getRequestDispatcher("result.jsp").forward(req, resp);

    } catch (NullPointerException | NumberFormatException e){
        req.setAttribute("error", "Not allowed parameters were sent");
        req.getRequestDispatcher("error.jsp").forward(req, resp);
    }

    }
    private void validate(String xString, String yString, String rString) throws NumberFormatException {
        int x = Integer.parseInt(xString);
        double y = Double.parseDouble(yString);
        int r = Integer.parseInt(rString);

        int[] xValues = new int[] { -3, -2, -1, 0, 1, 2, 3, 4, 5 };
        int[] rValues = new int[] { 1, 2, 3, 4, 5 };

        if (!(y > -3 && y < 3 && Arrays.stream(xValues).anyMatch(t -> t == x) && Arrays.stream(rValues).anyMatch(t -> t == r))) throw new NullPointerException();

    }
}
