package web.controllers;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "ControllerServlet", value = "/ControllerServlet")
public class ControllerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!req.getParameter("r").equals("-10") && !req.getParameter("x").equals("-10") && req.getParameter("y")!= null){
            req.getRequestDispatcher("/AreaCheckServlet").forward(req, resp);
        } else {
            req.setAttribute("error", "Some of yours fields are empty");
            req.getRequestDispatcher("error.jsp").forward(req, resp);
        }
    }
}
