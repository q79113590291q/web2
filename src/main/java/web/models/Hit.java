package web.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hit implements Serializable {

    private final int x;
    private final double y;
    private final int r;
    private final boolean hit;
    private final Date date;
    private final SimpleDateFormat formatter = new SimpleDateFormat("hh:mm yyyy-MM-dd");
    private final long time;

    public Hit(int x, double y , int r, long time){
        this.x = x;
        this.y = y;
        this.r = r;
        HitCheck checker = new HitCheck();
        this.hit = checker.check(x, y, r);
        this.date = new Date();
        this.time = time;
    }

    public int getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getR() {
        return r;
    }

    public boolean isHit() {
        return hit;
    }

    public String getDate() {
        return formatter.format(date);
    }

    public String getTime() {
        return (time/(1000000.0)+ "").substring(0,4);
    }
}
