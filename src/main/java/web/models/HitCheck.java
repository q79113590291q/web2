package web.models;

public class HitCheck {
    public boolean check(int x, double y, int r) {
        if (x < 0 && y < 0) return false; //3 quarter
        else if ((x < 0 && y > 0) && (x >= -r && y <= r)) return true; //2 quarter
        else if ((x >= 0 && y >= 0) && (x * x + y * y <= r * r)) return true; //1 quarter
        else return (x > 0 && y < 0) && (y >= -r / 2 + x); // 4 quarter
    }
}
