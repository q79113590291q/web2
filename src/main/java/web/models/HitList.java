package web.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HitList implements Serializable {
    private final List<Hit> list = new ArrayList<>();

    public int add(Hit hit){
        list.add(hit);
        return list.indexOf(hit);
    }

    public List<Hit> show(){
        return list;
    }

    public Hit getById(int id){
        return list.get(id);
    }
}
