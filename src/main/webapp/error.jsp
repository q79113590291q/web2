<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ERROR</title>
    <link rel="stylesheet" href="styles/result.css" type="text/css"/>
</head>
<body>
<div class="container">
    <p>
        ${error}
    </p>
    <p>
        <a href="index.jsp">Backward</a>
    </p>
</div>
</body>
</html>
