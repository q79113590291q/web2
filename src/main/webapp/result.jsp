<%@ page import="web.models.Hit" %>
<%@ page import="web.models.HitList" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Result</title>
    <link rel="stylesheet" href="styles/result.css" type="text/css"/>
</head>
<body>
    <div class="container">
        <h1>Result</h1>
            <% int id = (int) request.getAttribute("id");
                HitList list =  (HitList) config.getServletContext().getAttribute("list");
                Hit hit = list.getById(id);
                String hitString;
                if(hit.isHit()){
                    hitString = "Hit successfully";
                } else {
                    hitString = "Missed";
                }
            %>
        <p> <%=hitString%> </p>
        <p> X: <%=hit.getX()%> </p>
        <p> Y: <%=hit.getY()%> </p>
        <p> R: <%=hit.getR()%> </p>
        <p> Request time: <%=hit.getDate()%>[Moscow] </p>
        <p> Point is inside: <%=hit.isHit()%> </p>
        <p>Time of server processing: <%=hit.getTime()%>[ms]</p>
        <p>
            <a href="index.jsp"> Backward </a>
        </p>
    </div>
</body>
</html>
