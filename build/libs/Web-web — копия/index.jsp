
<%@ page import="web.models.HitList" %>
<%@ page import="web.models.Hit" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="magic" uri="http://custom/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>gosuslugi</title>
    <link rel="stylesheet" href="styles/header.css" type="text/css"/>
    <link rel="stylesheet" href="styles/table.css" type="text/css">
    <link rel="stylesheet" href="styles/style.css" type="text/css">
    <script src="js/script.js" ></script>
    <script type="text/javascript">
        window.addEventListener('keydown',function(e){if(e.keyIdentifier=='U+000A'||e.keyIdentifier=='Enter'||e.keyCode==13){if(e.target.nodeName=='INPUT'&&e.target.type=='text'){e.preventDefault();return false;}}},true);
        </script>
</head>
<body>
<table class="table">
    <tr class="header">
        <th colspan="2">
            <magic:rainbowTag value="Дорофеев Николай Р32101 Вариант 1109"/> Дорофеев Николай Р32101 Вариант 1109
        </th>
    </tr>
    <tr class="middle">
        <th width="40%">
            <div class="error" id="error1" style="opacity: 0; display: none;">
                Ошибка ввода
            </div>
            <form class="form" id="form" name="form" action="ControllerServlet" method="post">
                <label for="x" class="input">
                    <input type = "hidden" id = "x" name="x">
                    Enter X:
                    <p id ="x-block">
                        <input type="checkbox" id = "x-3" onchange="setX(-3, this)">
                        <label for="x-3">-3</label>
                        <input type="checkbox" id = "x-2" onchange="setX(-2, this)">
                        <label for="x-2">-2</label>
                        <input type="checkbox" id = "x-1" onchange="setX(-1, this)">
                        <label for="x-1">-1</label>
                        <input type="checkbox" id = "x0" onchange="setX(0, this)">
                        <label for="x0">0</label>
                        <input type="checkbox" id = "x1" onchange="setX(1, this)">
                        <label for="x1">1</label>
                        <input type="checkbox" id = "x2" onchange="setX(2, this)">
                        <label for="x2">2</label>
                        <input type="checkbox" id = "x3" onchange="setX(3, this)">
                        <label for="x3">3</label>
                        <input type="checkbox" id = "x4" onchange="setX(4, this)">
                        <label for="x4">4</label>
                        <input type="checkbox" id = "x5" onchange="setX(5, this)">
                        <label for="x5">5</label>
                    </p>
                </label>

                <label for="y" class="input">
                    Enter Y:
                    <input type="text" class="select" name="y" id="y" placeholder="(-3..3)" maxlength=7 autocomplete="off"/>
                </label>

                <label for="r" class="input">
                    Enter R:
                    <input type = "hidden" id = "r" name="r">
                    <p id="r-block">
                        <input type="checkbox" id ="r1" onchange="setR(1, this)">
                        <label for="r1">1</label>
                        <input type="checkbox" id ="r2" onchange="setR(2, this)">
                        <label for="r2">2</label>
                        <input type="checkbox" id ="r3" onchange="setR(3, this)">
                        <label for="r3">3</label>
                        <input type="checkbox" id ="r4" onchange="setR(4, this)">
                        <label for="r4">4</label>
                        <input type="checkbox" id ="r5" onchange="setR(5, this)">
                        <label for="r5">5</label>
                    </p>
                </label>

                <label class="button">
                    <input type="button" class="Submit" id="button" value="Submit" onclick="sendForm()"/>
                </label>
            </form>      
        </th>
        <th width="60%">
            <canvas id="graph" height="330" width="330" >
            <p>Ваш браузер не поддерживает рисование.</p>
            </canvas>
        </th>
    </tr>
    <tr class="ending">
        <td colspan="2" class="result">
            <table class="result-table" width="100%">
                <tr class="table-header">
                    <th>X</th>
                    <th>Y</th>
                    <th>R</th>
                    <th>Request time</th>
                    <th>Result</th>
                    <th>Time</th>
                </tr>
                <%
                    HitList list =  (HitList) config.getServletContext().getAttribute("list");
                    if (list != null) {
                        for (Hit hit : list.show()) {
                            out.println("<tr>");
                            out.println("<td>" + hit.getX() + "</td>");
                            out.println("<td>" + hit.getY() + "</td>");
                            out.println("<td>" + hit.getR() + "</td>");
                            out.println("<td>" + hit.getDate() + "[Moscow]" + "</td>");
                            out.println("<td>" + hit.isHit() + "</td>");
                            out.println("<td>" + hit.getTime() + "[ms]" + "</td>");
                            out.println("</tr>");
                        }
                    }
                %>
            </table>

        </td>
    </tr>
    <script>
        start();
    </script>
</table>
</body>
</html>