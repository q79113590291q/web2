<%--
  Created by IntelliJ IDEA.
  User: poouo
  Date: 24.10.2022
  Time: 00:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ERROR</title>
    <link rel="stylesheet" href="styles/result.css" type="text/css"/>
</head>
<body>
<div class="container">
    <p>
        ${error}
    </p>
    <p>
        <a href="index.jsp">Backward</a>
    </p>
</div>
</body>
</html>
