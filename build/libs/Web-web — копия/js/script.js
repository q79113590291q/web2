let button;
let y = -100;
let x = -10;
let xcheck;
let r = -10;
let rcheck;
let ycheck;
let img = new Image;
let err;
img.src = "https://i.ibb.co/WynsSB7/svg.png";
let context;


function start(){
    button = document.getElementById("button");
    ycheck = document.getElementById("y");
    xcheck = document.getElementById("x-block");
    rcheck = document.getElementById("r-block");
    ycheck.addEventListener("change", clearY);
    xcheck.addEventListener("click", clear);
    rcheck.addEventListener("click", clear);

    const canvas = document.getElementById('graph');
    canvas.addEventListener('mousedown', function(e) {
        getCursorPosition(canvas, e)
    })

}



function clearRButtons(){
    let Buttons= document.getElementById("r-block").getElementsByTagName("input");
    let Button=0;
    while(Button<Buttons.length){
        if(Buttons[Button].getAttribute("type")==="checkbox"){
            Buttons[Button].checked = false;
        }
        Button++;
    }
}

function clearXButtons(){
    let Buttons= document.getElementById("x-block").getElementsByTagName("input");
    let Button=0;
    while(Button<Buttons.length){
        if(Buttons[Button].getAttribute("type")==="checkbox"){
            Buttons[Button].checked = false;
        }
        Button++;
    }
}

function setR(newR, element) {
    clearRButtons();
    element.checked = true;
    r = newR;
}

function setX(newX, element) {
    clearXButtons();
    element.checked = true;
    x = newX;
}


function clear(){
    err = document.getElementById("error1");
    err.style.opacity = "0";
}

function clearY(){
    clear();
    ycheck = document.getElementById("y");
    y = ycheck.value.valueOf();
}

function graph(){
    window.onload = function(){
        let graphycs = document.getElementById("graph");
        if(graphycs && graphycs.getContext) {
            context = graphycs.getContext('2d');
            context.drawImage(img,0,0, 330, 330);
        }
    }
}


function getCursorPosition(canvas, event) {
    if (r !== -10){
        const rect = canvas.getBoundingClientRect()
        const xGraph = event.clientX - rect.left
        const yGraph = event.clientY - rect.top
        context.clearRect(0,0,330,330)
        context.drawImage(img,0,0,330,330);
        context.fillRect(xGraph,yGraph,3,3);
        context.fill();
        x = (xGraph - 165) * r / 120;
        y = -(yGraph - 165) * r / 120;
        showX(x);
        showY(y);
    }
}

function showX(x){
    x = Math.round(x);
    switch (x){
        case -3:
            setX(-3,document.getElementById("x-3"))
            break;
        case -2:
            setX(-2,document.getElementById("x-2"))
            break;
        case -1:
            setX(-1,document.getElementById("x-1"))
            break;
        case 0:
            setX(0,document.getElementById("x0"))
            break;
        case 1:
            setX(1,document.getElementById("x1"))
            break;
        case 2:
            setX(2,document.getElementById("x2"))
            break;
        case 3:
            setX(3,document.getElementById("x3"))
            break;
        case 4:
            setX(4,document.getElementById("x4"))
            break;
        case 5:
            setX(5,document.getElementById("x5"))
            break;
    }
}

function showY(y){
    y = y.toFixed(3);
    document.getElementById("y").value = y;
}

function sendForm(){
    let form = document.getElementById("form");
    let xElem = document.getElementById("x");
    let rElem = document.getElementById("r");
    xElem.value = x;
    rElem.value = r;

    err = document.getElementById("error1");
    console.log(x.valueOf(), y.valueOf(), r.valueOf());
    if (y.valueOf() > -3 && y.valueOf() < 3 && x !== -10 && r !== -10){
        form.submit();
    }
    else {
        err.style.opacity = "1";
        err.style.display = "inline";
    }
}

graph();